<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::get('login', 'LoginController@login')->name('login');

    Route::group(['prefix' => 'book'], function () {
        Route::get('/exists', 'BookController@existBook');

        Route::get('/notexists', 'BookController@notExistBook');

        Route::get('/more/ten', 'BookController@moreThanTen');



        Route::post('/create', 'BookController@store');

        Route::post('/edit/{book}', 'BookController@update');

        Route::get('/delete/{book}', 'BookController@destroy');
    });

});