<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    /** Table name @var string  */
    protected $table = 'books';

    /**
     * mass assignable attributes array for model
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'amount'
    ];

    /**
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get row where amount is greater than 0
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyExistsBooks()
    {
        return $this->scopeWhereAmount(0, '>');
    }

    /**
     * Get row where amount is equal 0
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyNotExistsBooks()
    {
        return $this->scopeWhereAmount(0, '=');
    }

    /**
     * Get row where amount is greater or equal than ten
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMoreThanTen()
    {
        return $this->scopeWhereAmount(10, '>=');
    }

    /**
     * @param int $amount
     * @param string $operator
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereAmount(int $amount, string $operator): \Illuminate\Database\Eloquent\Builder
    {
        return $this->where('amount', $operator, $amount);
    }


}
