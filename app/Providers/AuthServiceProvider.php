<?php

namespace App\Providers;

use Carbon\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

//        Passport::routes(function ($router) {
//            $router->forAccessTokens();//enable us to create access tokens
//            $router->forPersonalAccessTokens();//enable us to create personal tokens
//            $router->forTransientTokens();//creates the route for refreshing tokens
//        });
//
//        Passport::tokensExpireIn(Carbon::now()->addMinutes(10));
//
//        Passport::refreshTokensExpireIn(Carbon::now()->addDays(10));

        Passport::routes();
//
//        Passport::enableImplicitGrant();
//
//        Passport::tokensExpireIn(Carbon::now()->addDays(15));
//
//        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }
}
