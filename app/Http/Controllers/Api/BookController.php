<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Book;
use App\Http\Resources\BookCollection;
use App\Http\Requests\BookPost;

class BookController extends ApiController
{

    /**
     * Show books only if amount greater than 0
     *
     * @return BookCollection
     */
    public function existBook(): BookCollection
    {
        return new BookCollection(Book::onlyExistsBooks()->get());
    }


    /**
     * Show books only if amount is equal 0
     *
     * @return BookCollection
     */
    public function notExistBook(): BookCollection
    {
        return new BookCollection(Book::onlyNotExistsBooks()->get());
    }

    /**
     * Show books only if amount is greater or equal ten
     *
     * @return BookCollection
     */
    public function moreThanTen(): BookCollection
    {
        return new BookCollection(Book::moreThanTen()->get());
    }


    /**
     * @param BookPost $request
     * @param Book $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BookPost $request, Book $book)
    {
        try {
            $book->update($request->all());
            $statusCode = self::STATUS_CODE_OK;

            $response = [
                'success' => true,
                'message' => \Lang::get('api.update_success')
            ];
        } catch (\Exception $e) {
            $statusCode = self::STATUS_CODE_ERROR;

            $response = [
                'success' => false,
                'message' => \Lang::get('api.update_error')
            ];
        } finally {
            return response()->json($response, $statusCode);
        }
    }


    /**
     * @param BookPost $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BookPost $request): \Illuminate\Http\JsonResponse
    {
        try {
            Book::create($request->all());
            $statusCode = self::STATUS_CODE_OK;

            $response = [
                'success' => true,
                'message' => \Lang::get('api.store_success')
            ];
        } catch (\Exception $e) {
            $statusCode = self::STATUS_CODE_ERROR;

            $response = [
                'success' => false,
                'message' => \Lang::get('api.store_error')
            ];
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    /**
     * @param Book $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Book $book): \Illuminate\Http\JsonResponse
    {
        try {
            $book->delete();
            $statusCode = self::STATUS_CODE_OK;

            $response = [
                'success' => true,
                'message' => \Lang::get('api.destroy_success')
            ];
        } catch (\Exception $e) {
            $statusCode = self::STATUS_CODE_ERROR;

            $response = [
                'success' => false,
                'message' => \Lang::get('api.destroy_error')
            ];
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
