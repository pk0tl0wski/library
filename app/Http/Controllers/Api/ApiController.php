<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


class ApiController extends Controller
{

    /** Status Code OK */
    const STATUS_CODE_OK = 200;

    /** Status Code ERROR */
    const STATUS_CODE_ERROR = 404;


}