<?php

use Illuminate\Database\Seeder;
use App\Book;


class BookTableSeeder extends Seeder
{

    const SAMPLE_DATA = [
        [
            'title' => 'Book 1',
            'amount' => 0
        ],
        [
            'title' => 'Book 2',
            'amount' => 5
        ],
        [
            'title' => 'Book 3',
            'amount' => 12
        ],
        [
            'title' => 'Book 4',
            'amount' => 2
        ],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::SAMPLE_DATA as $data) {
            Book::create($data);
        }
    }
}
