<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Hash as Hash;

class UserTableSeeder extends Seeder
{

    const DEFAULT_USER = [
        'name' => 'api',
        'email' => 'api@gmail.com',
        'password' => 'api'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = self::DEFAULT_USER;
        $credentials['password'] = Hash::make($credentials['password']);
        \App\User::create($credentials);


    }
}
