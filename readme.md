
Kontrolery :
  - ApiController - główny kontroler API - dziedziczy bezpośrednio po App\Http\Controller
  - BookController - kontroler do zarządzania książkami, dziedziczy po ApiController
  
Do walidacji danych stworzono i użyto Requests\BookPost.php w którym użyto reguł
walidacyjnych w celu kontroli nad danymi wejściowymi

Do zwracania listy książaek stworzono i użyto resource oraz resourceCollection 


Model:
 - Book - model tabeli "książki"
 - User - model użytkownika - aktualnie nie używany na potrzeby systemu, jednak zachowany
        w celu jego poprawnego działania
 
Wiadomości informacyjne, użyte w kontrolerach w celu poinformowania użytkownika
o pomyślnej modyfikacji danych bądź też negatywnej zostały umieszczone w pliku
językowym resources/lang/pl/api.php

