<?php
return [
    'update_success' => 'Pomyślnie zedytowano dane',
    'update_error' => 'Wystąpił problem',

    'destroy_success' => 'Pomyślnie usunięto',
    'destroy_error' => 'Wystąpił problem',

    'store_success' => 'Pomyślnie dodanie dane',
    'store_error' => 'Wystąpił problem'
];